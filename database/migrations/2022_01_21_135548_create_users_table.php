<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->unique();
            $table->string('first_name', 50);
            $table->string('email', 150)->unique();
            $table->string('last_name', 50);
            $table->string('password', 100)->unique(); // in most where condition used
            $table->date('date_of_birth')->nullable();
            $table->string('profile_image', 200)->nullable();
            $table->enum('role', ['ADMIN', 'USER'])->default('ADMIN');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
