<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Helpers\ResponseTrait;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exception\JWTException;


class AdminAuthController extends Controller
{
    use ResponseTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    /**
     * @author Shweta
     * @param Request $request
     * @return 
     */
    public function store(Request $request) {
        try {
            $alreadyExists = User::where('email', $request->email)->first();
            if ($alreadyExists) {
                return $this->error('ALREADY_EXISTS');
            }
            $user = User::createAdminUser($request);
            return $this->success($user, 'CREATE_SUCCESS', 200);
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

     /**
     * @author Shweta
     * @param Request $request
     * @return 
     */
    public function login(Request $request) {
        try {
            $user = User::where('email', $request->email)->first();
            $this->checkAuthorization($user, $request); 
            $token = JWTAuth::attempt($request->all());
            dd($token);
            return response()->json([
                'token' => $token
            ]);
            
        } catch (\Exception $e) {
            return $this->error($e->getMessage());
        }
    }

    public function checkAuthorization($user, $request) {
        $error = [];
        try {
            if(!$user){
                throw new \Exception('USER_DOESNT_EXISTS');
            } elseif(!Hash::check($request->get('password'), $user->password)){
                throw new \Exception('INVALID_CREDENTIALS');
            } elseif($user->deleted_at!==null){
                throw new \Exception('USER_ACCOUNT_DELETED');
            }
        } catch(\Exception $e){
            $error = ['message'=>$e->getMessage(), 'code' => 422, 'status' => false];
        }
        return $error;
    }
    //
}
