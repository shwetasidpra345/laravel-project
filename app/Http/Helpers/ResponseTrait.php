<?php

namespace App\Http\Helpers;

use Illuminate\Http\JsonResponse;

trait ResponseTrait
{
    /**
     * @author Shweta
     * @param object $data
     * @param string $msg
     * @param integer $code
     * @return 
     */
    public function error($msg, $code=422) {
        $msg = trans('error.'.$msg, [], 'en');
        return response()->json(['message'=>$msg,'code'=>$code]);
    }
    //
    public function success($data, $msg, $code=200) {
        $msg = trans('message.'.$msg, [], 'en');
        return response()->json(['data' => $data, 'message'=>$msg,'code'=>$code]);
    }
    //
}
