<?php 
return [
    'SUCCESS' => 'Success',
    'CREATE_SUCCESS' => 'User registered successfully',
    'USER_ACCOUNT_DELETED' => 'User account deleted.'
];
?>